package com.itential.demo.portturnup;

import com.itential.demo.portturnup.namespaces.*;
import java.util.List;
import java.util.Properties;
import com.tailf.conf.*;
import com.tailf.navu.*;
import com.tailf.ncs.ns.Ncs;
import com.tailf.dp.*;
import com.tailf.dp.annotations.*;
import com.tailf.dp.proto.*;
import com.tailf.dp.services.*;
import com.tailf.ncs.template.Template;
import com.tailf.ncs.template.TemplateVariables;

import com.tailf.ncs.ResourceManager;
import com.tailf.ncs.annotations.Resource;
import com.tailf.ncs.annotations.ResourceType;
import com.tailf.ncs.annotations.Scope;
import com.tailf.cdb.Cdb;
import com.tailf.cdb.CdbDBType;
import com.tailf.ncs.ns.Ncs;

import org.apache.log4j.Logger;

public class portTurnUpRFS {
    // Use the ResourceManager to inject an instance of the Cdb
    @Resource(type=ResourceType.CDB, scope=Scope.CONTEXT, qualifier="link-oper-check")
    private Cdb cdb;
    private static Logger log  = Logger.getLogger(portTurnUpRFS.class);


    /**
     * Create callback method.
     * This method is called when a service instance committed due to a create
     * or update event.
     *
     * This method returns a opaque as a Properties object that can be null.
     * If not null it is stored persistently by Ncs.
     * This object is then delivered as argument to new calls of the create
     * method for this service (fastmap algorithm).
     * This way the user can store and later modify persistent data outside
     * the service model that might be needed.
     *
     * @param context - The current ServiceContext object
     * @param service - The NavuNode references the service node.
     * @param ncsRoot - This NavuNode references the ncs root.
     * @param opaque  - Parameter contains a Properties object.
     *                  This object may be used to transfer
     *                  additional information between consecutive
     *                  calls to the create callback.  It is always
     *                  null in the first call. I.e. when the service
     *                  is first created.
     * @return Properties the returning opaque instance
     * @throws ConfException
     */

    @ServiceCallback(servicePoint="itential-demo-port-turn-up-servicepoint",
        callType=ServiceCBType.CREATE)
    public Properties create(ServiceContext context,
                             NavuNode service,
                             NavuNode ncsRoot,
                             Properties opaque)
                             throws ConfException {

        log.info("itential-demo-port-turn-up >> CREATE::context:"+context.toString()+"~service"+service+"~ncsRoot"+ncsRoot+"~opaque"+opaque);

        for (String vlan : service.leaf("allowed-vlan").valueAsString().split(",")){
            log.debug("itential-demo-port-turn-up >> this is the vlan:"+vlan);

            //create instance of template
            Template myTemplate = new Template(context, "itential-demo-port-turn-up-template");
            TemplateVariables myVars = new TemplateVariables();


            if(vlan.contains("-")){
                log.debug("itential-demo-port-turn-up >> found range, need to loop each as int");


                for (int i=Integer.parseInt(vlan.split("-")[0]); i<Integer.parseInt(vlan.split("-")[1]); i++){
                    log.debug("itential-demo-port-turn-up >> found range, need to loop each as int.  each is:"+i);
                    try {
                        // apply the template with the variable
                        myVars.putQuoted("VLAN",String.valueOf(i));
                        myTemplate.apply(service, myVars);
                    } catch (Exception e) {
                        throw new DpCallbackException(e.getMessage(), e);
                    }
                }



            } else {
                log.debug("itential-demo-port-turn-up >> did not find range, not going to loop each");

                try {
                    // apply the template with the variable
                    myVars.putQuoted("VLAN",vlan);
                    myTemplate.apply(service, myVars);

                } catch (Exception e) {
                    throw new DpCallbackException(e.getMessage(), e);
                }

            }
        }

        return opaque;
    }
}

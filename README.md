## _Deprecation Notice_
This Pre-Built has been deprecated as of 05-02-2024 and will be end of life on 05-02-2025. The capabilities of this Pre-Built have been replaced by the [Cisco - NSO](https://gitlab.com/itentialopensource/pre-built-automations/cisco-nso)

# Service Provisioning

## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

The Service Provisioning pre-built demonstrates an end-to-end usage of the Service Management workflow tasks in the Itential Automation Platform (IAP).

This pre-built allows the user to run the workflow in either **Demo**, **Normal**, **Verbose** or **Zero touch** modes.

For **Demo** mode, this pre-built has no prerequisites, and all south-bound system integrations have been replaced with hard-coded values in the flow.

For running this pre-built in any of the other modes, please follow the list of prerequisites first:

* Cisco NSO installation 
* Cisco IOS NED installed
* Itential-Tools (NSO package) installed
* Connected IAP NSO adapter with both device and service brokers assignment. (follow <a href="http://docs.itential.io/admin/Itential%20Automation%20Platform/Integrations/Network%20Adapters/#nso" target="_blank">THIS</a> link for configuration examples)
* A Cisco-IOS device (or a Network Simulated device also known as "netsim"): must be on-boarded and in-sync with NSO
* Itential-demo-port-turn-up service model compiled and loaded in NSO (service model can be found <a href="https://gitlab.com/itentialopensource/pre-built-automations/service-provisioning/-/tree/master/assets/service-model" target="_blank">HERE</a>)

## Installation Prerequisites

Users must satisfy the following prerequisites:

* Itential Automation Platform: `^2021.2`

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section. 
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.

## How to run

Use the following steps to run the pre-built:
1. Once the pre-built is installed as outlined in the [How to Install](#how-to-install) section above, go to `Operations Manager` and run the `Service Provisioning` automation. 
2. In the `Modes` dropdown which is the only input as part of the JSON form, choose either `Demo`, `Normal`, `Verbose` or `Zero Touch`. 

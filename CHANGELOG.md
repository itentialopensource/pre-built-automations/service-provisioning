
## 0.4.6 [07-12-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/service-provisioning!39

2024-07-12 19:41:46 +0000

---

## 0.4.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/service-provisioning!38

---

## 0.4.4 [05-24-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/service-provisioning!37

---

## 0.4.3 [02-14-2022]

* 2021.2 certifications

See merge request itentialopensource/pre-built-automations/service-provisioning!36

---

## 0.4.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/service-provisioning!35

---

## 0.4.1 [03-19-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/service-provisioning!34

---

## 0.4.0 [07-16-2020]

* [minor/LB-404] Switch absolute path to relative path for img

See merge request itentialopensource/pre-built-automations/service-provisioning!30

---

## 0.3.0 [06-17-2020]

* Update manifest and readme to reflect gitlab

See merge request itentialopensource/pre-built-automations/Service-Provisioning!29

---

## 0.2.12 [05-14-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/service-provisioning!27

---

## 0.2.11 [04-17-2020]

* including adapters in IAPDependencies

See merge request itentialopensource/pre-built-automations/service-provisioning!26

---

## 0.2.10 [04-17-2020]

* including adapters in IAPDependencies

See merge request itentialopensource/pre-built-automations/service-provisioning!26

---

## 0.2.9 [04-10-2020]

* including adapters in IAPDependencies

See merge request itentialopensource/pre-built-automations/service-provisioning!26

---

## 0.2.8 [04-08-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/service-provisioning!23

---

## 0.2.7 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/service-provisioning!20

---

## 0.2.6 [03-17-2020]

* update README link from confluence to docs site

See merge request itentialopensource/pre-built-automations/service-provisioning!17

---

## 0.2.5 [03-04-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/service-provisioning!15

---

## 0.2.4 [02-26-2020]

* updated broken png links

See merge request itentialopensource/pre-built-automations/service-provisioning!13

---

## 0.2.3 [02-26-2020]

* Patch/separate workflows for 2019.3 release

See merge request itentialopensource/pre-built-automations/service-provisioning!12

---

## 0.2.2-2019.3.0 [01-23-2020]

* version bumping IAP dependencies to match release/2019.3

See merge request itentialopensource/pre-built-automations/service-provisioning!11

---

## 0.2.2 [01-16-2020]

* resize images and reformat markdown to align with linting rules

See merge request itentialopensource/pre-built-automations/service-provisioning!8

---

## 0.2.1 [12-03-2019]

* Patch 1

See merge request itentialopensource/pre-built-automations/service-provisioning!6

---

## 0.2.0 [08-06-2019]

* [minor/DSUP-685]Update IAPDependencies to use partial ranges rather than semver ranges

See merge request itentialopensource/pre-built-automations/service-provisioning!5

---
## 0.1.2 [08-01-2019]
* Update .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/service-provisioning!4

---

## 0.1.1 [08-01-2019]
* Artifact matches current App-Artifacts version in package.json IAP dependencies

See merge request itentialopensource/pre-built-automations/service-provisioning!3

---

## 0.1.0 [07-18-2019]
* Update package.json to include IAPDep... and new keyword

See merge request itentialopensource/pre-built-automations/service-provisioning!1

---

## 0.0.2 [05-07-2019]
* Bug fixes and performance improvements

See commit 803a50b

---\n\n\n
